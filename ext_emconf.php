<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2015 IMIA net based solutions (info@imia.de)
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

$EM_CONF[$_EXTKEY] = array(
	'title' => 'IMIA T3UM',
	'description' => 'TYPO3 Update Manager',
	'category' => 'misc',
	'author' => 'David Frerich',
	'author_email' => 'd.frerich@imia.de',
	'author_company' => 'IMIA net based solutions',
	'state' => 'alpha',
	'uploadfolder' => false,
	'createDirs' => '',
	'clearCacheOnLoad' => true,
	'version' => '0.3.0',
	'constraints' => array(
		'depends' => array(
			'php' => '5.3.6-0.0.0',
			'typo3' => '6.2.0-7.6.999',
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
);