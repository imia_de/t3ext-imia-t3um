<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2015 IMIA net based solutions (info@imia.de)
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

if (TYPO3_MODE === 'BE' ) {
    $extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][$_EXTKEY]);
    
    if ($extConf['devMode'] || \TYPO3\CMS\Core\Utility\GeneralUtility::getApplicationContext()->isDevelopment()) {
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
            'IMIA.' . $_EXTKEY,
            'system',
            't3um',
            '',
            array (
                'Backend'   => 'index, register, deploy, extensionMissmatch',
            ),
            array (
                'access'    => 'admin',
                'icon'      => 'EXT:' . $_EXTKEY . '/' . (\TYPO3\CMS\Core\Utility\GeneralUtility::compat_version('7.0')
                    ? 'ext_icon.svg'
                    : 'Resources/Public/Images/T3UM.png'),
                'labels'    => 'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/Module.xlf',
            )
        );
    }
}