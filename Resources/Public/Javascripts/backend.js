(function($) {
    $(document).ready(function(){
        $('form a.checkall').click(function(e){
            e.preventDefault();

            var checkboxes = $(this).closest('form').find('input[type="checkbox"]');
            if (checkboxes.filter(':checked').length == checkboxes.length) {
                checkboxes.prop('checked', false);
            } else {
                checkboxes.prop('checked', true);
            }

            return false;
        });
    });
})(jQuery);