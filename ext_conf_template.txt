# cat=basic/10; type=boolean; label=DEV-Mode: Enable Backend-Module for Registration and Deployment
devMode =

# cat=basic/20; type=boolean; label=Deployment: Enable Deployment on this Instance
deployment = 1

# cat=basic/30; type=string; label=Service URL:Url of the TYPO3 Update Manager
serviceURL =

# cat=basic/40; type=string; label=Auth for Keyed-Hash Message Authentication
auth =

# cat=basic/50; type=string; label=Instanzen und Domains
instances = 