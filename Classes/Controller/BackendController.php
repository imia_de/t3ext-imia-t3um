<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2015 IMIA net based solutions (info@imia.de)
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

namespace IMIA\ImiaT3um\Controller;

use IMIA\ImiaT3um\Exception\Exception;
use IMIA\ImiaT3um\Utility\Api\HttpClient;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_t3um
 * @subpackage  Controller
 * @author      David Frerich <d.frerich@imia.de>
 */
class BackendController extends AbstractController
{
    /**
     * @var array
     */
    protected $extConf;

    /**
     * @var array
     */
    protected $info;

    /**
     * @var HttpClient
     */
    protected $httpClient;
    
    public function initializeAction()
    {
        if (!$GLOBALS['BE_USER']->isAdmin()) {
            throw new \RuntimeException('TYPO3 Fatal Error: You have tried to access T3UM as non admin. Access prohibited!');
        }
        
        $this->getExtConf();
        $this->extConf['registered'] = false;
        $this->extConf['connected'] = false;
        if ($this->extConf['serviceURL'] && $this->extConf['auth']) {
            $this->extConf['registered'] = true;
            
            list($publicId, $secret) = explode(':', $this->extConf['auth']);
            $this->httpClient = GeneralUtility::makeInstance('IMIA\ImiaT3um\Utility\Api\HttpClient',
                $this->extConf['serviceURL'], (int)$publicId, $secret);
            
            
            $info = $this->httpClient->getInfo();
            if ($info && $info['project']) {
                $this->extConf['connected'] = true;
                $this->info = $info;
            }
        }
    }

    /**
     * @param \TYPO3\CMS\Extbase\Mvc\View\ViewInterface $view
     */
    protected function initializeView($view)
    {
        parent::initializeView($view);

        $this->view->assign('extConf', $this->extConf);
        $this->controllerContext->getFlashMessageQueue()->getAllMessagesAndFlush();

        if (!function_exists('apache_request_headers')) {
            $this->view->assign('missingHeaders', true);
        }
    }
    
    /**
     */
    public function indexAction()
    {
        if ($this->extConf['connected']) {
            $this->view->assign('instances', $this->getInstances());
        }
    }

    public function deployAction()
    {
        $instances = array();
        if ($this->request->hasArgument('instances')) {
            $this->view->assign('instanceIds', $this->request->getArgument('instances'));
            $instances = $this->getInstances($this->request->getArgument('instances'));
            foreach ($instances as $key => $instance) {
                if (!$instance['deployment']) {
                    unset($instances[$key]);
                }
            }
        }

        if (!$this->extConf['connected'] || count($instances) == 0) {
            $this->redirect('index');
        } else {
            $localExtensions = $this->getLocalExtension();
            foreach ($instances as $instance) {
                $instanceKey = 'instance_' . $instance['id'];
                foreach ($this->httpClient->getExtensions($instance) as $extension) {
                    if (array_key_exists($extension['key'], $localExtensions)) {
                        $localExtensions[$extension['key']][$instanceKey] = $extension;
                    } else {
                        $localExtensions[$extension['key']] = $extension;
                        unset($localExtensions[$extension['key']]['version']);
                        unset($localExtensions[$extension['key']]['checksum']);
                        unset($localExtensions[$extension['key']]['checksums']);
                        $localExtensions[$extension['key']][$instanceKey] = $extension;
                    }
                }
            }

            foreach ($this->httpClient->getTERExtensions(array_keys($localExtensions)) as $extension) {
                $localExtensions[$extension['key']]['ter'] = $extension;
            }

            $extensions = array();
            foreach ($localExtensions as $localExtension) {
                $extensions[$localExtension['title'] . '_' . $localExtension['key']] = $localExtension;
            }
            ksort($extensions);

            $typo3Core = json_decode(file_get_contents('http://get.typo3.org/json'), true);

            $this->view->assign('instances', $instances);
            $this->view->assign('extensions', $extensions);
            $this->view->assign('system', array(
                'version'   => TYPO3_version,
                'lts'       => $typo3Core['latest_lts'],
                'stable'    => $typo3Core['latest_stable'],
            ));
        }
    }

    public function extensionMissmatchAction()
    {
        if ($this->request->hasArgument('returnUrl')) {
            $returnUrl = $this->request->getArgument('returnUrl');
        } else {
            $returnUrl = $this->uriBuilder->uriFor('index');
        }

        $extension = null;
        if ($this->request->hasArgument('extension')) {
            $extKey = $this->request->getArgument('extension');

            $instance = null;
            if ($this->request->hasArgument('instance')) {
                $instanceId = $this->request->getArgument('instance');
                $instances = $this->getInstances(array($instanceId));
                if ($instances && is_array($instances) && count($instances) == 1) {
                    $instance = array_shift($instances);
                    foreach ($this->httpClient->getExtensions($instance) as $ext) {
                        if ($ext['key'] == $extKey) {
                            $extension = $ext;
                            break;
                        }
                    }

                    $this->view->assign('instance', $instance);
                }

                $compareType = 'dynamic';
            } else {
                $compareType = 'ter';
                $extension = $this->getLocalExtension($extKey);
            }
        }

        if (!$extension) {
            return $this->redirectToUri($returnUrl);
        } else {
            if (!is_array($extension['checksums'])) {
                $extension['checksums'] = json_decode($extension['checksums'], true);
            }

            $terExtensions = $this->httpClient->getTERExtensions(array($extension['key']));
            if ($terExtensions && is_array($terExtensions) && count($terExtensions) == 1) {
                $terExtension = array_shift($terExtensions);
                if (!is_array($terExtension['checksums'])) {
                    $terExtension['checksums'] = json_decode($terExtension['checksums'], true);
                }
            }

            $compareExtension = $terExtension;
            if (!$compareExtension && $compareType == 'dynamic') {
                $compareExtension = $this->getLocalExtension($extKey);
                $compareType = 'local';
            } else {
                $compareType = 'ter';
            }

            if ($compareExtension) {
                $newFiles = array();
                $changedFiles = array();
                foreach ($extension['checksums'] as $file => $checksum) {
                    if (!array_key_exists($file, $compareExtension['checksums'])) {
                        $newFiles[] = $file;
                    } else {
                        if ($checksum != $compareExtension['checksums'][$file]) {
                            $changedFiles[] = $file;
                        }
                    }
                }
                $missingFiles = array();
                foreach ($compareExtension['checksums'] as $file => $checksum) {
                    if (!array_key_exists($file, $extension['checksums'])) {
                        $missingFiles[] = $file;
                    }
                }

                $this->view->assign('newFiles', $newFiles);
                $this->view->assign('changedFiles', $changedFiles);
                $this->view->assign('missingFiles', $missingFiles);
                $this->view->assign('extension', $extension);
                $this->view->assign('returnUrl', $returnUrl);
                $this->view->assign('compareType', $compareType);
            } else {
                return $this->redirectToUri($returnUrl);
            }
        }
    }

    /**
     */
    public function registerAction()
    {
        if ($this->extConf['connected']) {
            $this->redirect('index');
        }
        
        $system = $GLOBALS['TYPO3_CONF_VARS']['SYS'];
        $this->view->assign('sys', $system);

        if ($this->request->getMethod() == 'POST') {
            $errors = array();
            
            if (!$this->request->hasArgument('serviceURL') || !trim($this->request->getArgument('serviceURL'))) {
                $errors['serviceURL'] = 'Bitte geben Sie die URL zum TYPO3 Update Manager ein.';
            } elseif (!GeneralUtility::isValidUrl($this->request->getArgument('serviceURL'))) {
                $errors['serviceURL'] = 'Bitte geben Sie eine gültige URL zum TYPO3 Update Manager ein.';
            }
            if (!$this->request->hasArgument('user') || !trim($this->request->getArgument('user'))) {
                $errors['user'] = 'Bitte geben Sie Ihren T3UM Benutzer ein.';
            }
            if (!$this->request->hasArgument('password') || !trim($this->request->getArgument('password'))) {
                $errors['password'] = 'Bitte geben Sie Ihr T3UM Passwort ein.';
            }
            
            if (count($errors)) {
                foreach ($errors as $field => $message) {
                    $this->flash($message, $field, FlashMessage::ERROR);
                }
            } else {
                $serviceURL = preg_replace('/\/$/ism', '', $this->request->getArgument('serviceURL'));
                $user = $this->request->getArgument('user');
                $password = $this->request->getArgument('password');
                
                $this->extConf['serviceURL'] = $serviceURL;
                $this->view->assign('extConf', $this->extConf);
                
                $httpClient = GeneralUtility::makeInstance('IMIA\ImiaT3um\Utility\Api\Registration\HttpClient', $serviceURL, $user, $password);
                
                try {
                    $response = $httpClient->register($system['sitename'], $system['encryptionKey']);
                    if ($response && $response['auth'] && $response['auth']['project']) {
                        $configurationUtility = $this->objectManager->get('TYPO3\CMS\Extensionmanager\Utility\ConfigurationUtility');
                        $newConfiguration = $configurationUtility->getCurrentConfiguration('imia_t3um');
                        
                        ArrayUtility::mergeRecursiveWithOverrule($newConfiguration, array(
                            'auth'          => array('value' => $response['auth']['id'] . ':' . $response['auth']['secret']),
                            'serviceURL'    => array('value' => $serviceURL),
                        ));

                        if (GeneralUtility::getApplicationContext()->isDevelopment()) {
                            $newConfiguration['devMode']['value'] = '0';

                            $extConfT3UM = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['imia_t3um']);
                            $extConfT3UM['devMode'] = '0';
                            $GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['imia_t3um'] = serialize($extConfT3UM);
                        }
                        
                        ob_start();
                        $configurationUtility->writeConfiguration(
                            $configurationUtility->convertValuedToNestedConfiguration($newConfiguration),
                            'imia_t3um'
                        );

                        ob_end_clean();

                        $this->redirect('index');
                    } else {
                        $this->flash('Registrierung fehlgeschlagen', 'TYPO3 Update Manager', FlashMessage::ERROR);
                    }
                } catch(Exception $e) {
                    if ($e->getObject()) {
                        $result = $e->getObject();

                        ob_start();
                        print_r($result['response'] ?: $result);
                        $error = ob_get_contents();
                        ob_end_clean();

                        $this->flash($error, 'TYPO3 Update Manager', FlashMessage::ERROR);
                    } else {
                        $this->flash($e->getMessage(), 'TYPO3 Update Manager', FlashMessage::ERROR);
                    }
                }
            }
        }
    }

    /**
     * @param array $ids
     * @return array
     */
    protected function getInstances($ids = null)
    {
        if ($ids) {
            $ids = array_map('intval', $ids);
        }

        $instances = array();
        foreach ($this->info['project']['instances'] as $instance) {
            $instance = $this->recursiveKeysToCamelCase($instance);
            if ($instance['healthCheckApi'] != 'ignore' && $instance['canonicalDomain']) {
                if (!$ids || in_array((int)$instance['id'], $ids)) {
                    $instances[] = $instance;
                }
            }
        }

        return $instances;
    }

    /**
     * @param mixed $data
     * @return mixed
     */
    protected function recursiveKeysToCamelCase($data)
    {
        if (is_array($data)) {
            $newData = array();
            foreach ($data as $key => $value) {
                $newData[GeneralUtility::underscoredToLowerCamelCase($key)] = $this->recursiveKeysToCamelCase($value);
            }

            return $newData;
        } else {
            return $data;
        }
    }

    /**
     * @param string $text
     * @param string $title
     * @param string $type
     */
    protected function flash($text, $title, $type) 
    {
        $flashMessage = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
            'TYPO3\\CMS\\Core\\Messaging\\FlashMessage', $text, $title, $type, TRUE
        );
        $this->controllerContext->getFlashMessageQueue()->enqueue($flashMessage);
    }
}