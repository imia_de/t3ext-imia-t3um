<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2015 IMIA net based solutions (info@imia.de)
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

namespace IMIA\ImiaT3um\Controller;

use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_t3um
 * @subpackage  Controller
 * @author      David Frerich <d.frerich@imia.de>
 */
abstract class AbstractController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * @var array
     */
    protected $extConf;

    /**
     * @var array
     */
    protected $extInfo;

    /**
     * @var array
     */
    protected $localExtensions;

    /**
     * @param string $extKey
     * @return array
     */
    protected function getLocalExtension($ext = null)
    {
        if (!$this->localExtensions) {
            $visibleExtFields = array(
                'title', 'description', 'category', 'version', 'state', 'author', 'author_email', 'author_company',
                'constraints', 'icon', 'installed', 'key', 'checksum', 'checksums',
            );

            $this->localExtensions = array();
            foreach ($this->getExtInfo() as $extKey => $extension) {
                if ($extension['type'] != 'System') {
                    $extension['checksums'] = $this->getExtensionChecksums($extension);
                    $extension['checksum'] = sha1(json_encode($extension['checksums']));
                    $extension['icon'] = $this->getExtensionIcon($extension);

                    foreach ($extension as $key => $val) {
                        if (!in_array($key, $visibleExtFields)) {
                            unset($extension[$key]);
                        }
                    }

                    $this->localExtensions[$extKey] = $extension;
                }
            }
        }

        if ($ext) {
            return $this->localExtensions[$ext];
        } else {
            return $this->localExtensions;
        }
    }

    /**
     * @return array
     */
    protected function getExtInfo()
    {
        if (!$this->extInfo) {
            $extListUtility = $this->objectManager->get('TYPO3\CMS\Extensionmanager\Utility\ListUtility');
            $this->extInfo =  $extListUtility->getAvailableAndInstalledExtensionsWithAdditionalInformation();
        }

        return $this->extInfo;
    }

    /**
     * @param array $extension
     * @return string
     */
    protected function getExtensionChecksums($extension)
    {
        $path = PATH_site . $extension['siteRelPath'];
        $files = $this->getFiles(substr($path, 0, -1));

        $checksums = array();
        foreach ($files as $file) {
            $fileName = str_replace($path, '', $file);
            if (!in_array($fileName, array('ext_emconf.php', 'composer.json'))) {
                $checksums[$fileName] = sha1(str_replace("\r\n", "\n", file_get_contents($file)));
            }
        }
        ksort($checksums);

        return $checksums;
    }


    /**
     * @param array $extension
     * @return string
     */
    protected function getExtensionIcon($extension)
    {
        $icon = null;
        if ($extension['ext_icon']) {
            $iconFile = PATH_site . $extension['siteRelPath'] . $extension['ext_icon'];
            $iconContents = @file_get_contents($iconFile);

            if ($iconContents) {
                $icon = base64_encode($iconContents);
            }
        }
        
        return $icon;
    }

    /**
     * @param string $path
     * @return array
     */
    protected function getFiles($path)
    {
        $files = array();
        foreach (scandir($path) as $file){
            if (strpos($file, '.') !== 0 ) {
                if (is_dir($path . '/' . $file)) {
                    $files = array_merge($files, $this->getFiles($path . '/' . $file));
                } else {
                    $files[] = $path . '/' . $file;
                }
            }
        }
        
        return $files;
    }

    /**
     * @param string $key
     * @return mixed
     */
    protected function getExtConf($key = null)
    {
        if (!$this->extConf) {
            $this->extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][
                GeneralUtility::camelCaseToLowerCaseUnderscored($this->extensionName)]);
        }
        
        if ($key) {
            if (array_key_exists($key, $this->extConf)) {
                return $this->extConf[$key];
            } else{
                return null;
            }
        } else {
            return $this->extConf;
        }
    }
}