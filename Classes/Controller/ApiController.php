<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2015 IMIA net based solutions (info@imia.de)
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

namespace IMIA\ImiaT3um\Controller;

use IMIA\ImiaT3um\Utility\Api\HmacSign;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_t3um
 * @subpackage  Controller
 * @author      David Frerich <d.frerich@imia.de>
 */
class ApiController extends AbstractController
{
    /**
     * @var string
     */
    protected $requestContent = '';

    /**
     * @var array
     */
    protected $requestArguments = array();

    /**
     * @var array
     */
    protected $requestHeaders = array();
    
    public function initializeAction()
    {
        $this->parseRequest();
        $this->checkHmacAuth();
    }

    public function infoAction()
    {
        $system = $GLOBALS['TYPO3_CONF_VARS']['SYS'];
        $extInfo = $this->getExtInfo();

        $this->render(array(
            'system'    => array(
                'name'      => $system['sitename'],
                'typo3'     => TYPO3_version,
                'env'       => $this->getEnv(),
            ),
            't3um'      => array(
                'version'       => $extInfo['imia_t3um']['version'],
                'deployment'    => $this->getExtConf('deployment') ? true : false,
            )
        ));
    }
    
    public function usersAction()
    {
        $users = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
            'username, admin, disable, starttime, endtime, email, crdate, realName AS name, lastlogin', 
            'be_users',
            'deleted = 0');

        $this->render(array(
            'users' => $users,
        ));
    }

    public function extensionsAction()
    {
        $this->render(array(
            'extensions' => $this->getLocalExtension(),
        ));
    }

    protected function parseRequest()
    {
        $this->requestArguments = $_REQUEST;
        unset($this->requestArguments['eID']);

        if (function_exists('apache_request_headers')) {
            $this->requestHeaders = apache_request_headers();
        } else {
            $this->requestHeaders = array(
                'Content-Type'      => isset($_SERVER['REDIRECT_CONTENT_TYPE']) ? $_SERVER['REDIRECT_CONTENT_TYPE'] : (isset($_SERVER['CONTENT_TYPE']) ? $_SERVER['CONTENT_TYPE'] : ''),
                'Content-Length'    => isset($_SERVER['REDIRECT_CONTENT_LENGTH']) ? $_SERVER['REDIRECT_CONTENT_LENGTH'] : (isset($_SERVER['CONTENT_LENGTH']) ? $_SERVER['CONTENT_LENGTH'] : ''),
                'Content-MD5'       => isset($_SERVER['REDIRECT_CONTENT_MD5']) ? $_SERVER['REDIRECT_CONTENT_MD5'] : (isset($_SERVER['CONTENT_MD5']) ? $_SERVER['CONTENT_MD5'] : ''),
                'Date'              => isset($_SERVER['REDIRECT_HTTP_DATE']) ? $_SERVER['REDIRECT_HTTP_DATE'] : (isset($_SERVER['HTTP_DATE']) ? $_SERVER['HTTP_DATE'] : ''),
                'Authorization'     => isset($_SERVER['REDIRECT_HTTP_AUTHORIZATION']) ? $_SERVER['REDIRECT_HTTP_AUTHORIZATION'] : (isset($_SERVER['HTTP_AUTHORIZATION']) ? $_SERVER['HTTP_AUTHORIZATION'] : ''),
            );
        }

        if (in_array($_SERVER['REQUEST_METHOD'], array('POST', 'PUT', 'PATCH'))) {
            $this->requestContent = file_get_contents('php://input');

            if ($this->requestContent) {
                $data = null;

                switch ($this->requestHeaders['Content-Type']) {
                    case 'application/json':
                        $data = json_decode($this->requestContent, true);

                        if (json_last_error() != JSON_ERROR_NONE) {
                            unset($data);
                        }
                        break;
                    case 'application/x-www-form-urlencoded':
                        parse_str($this->requestContent, $data);
                        break;
                }
                
                if ($data && is_array($data)) {
                    $this->args = array_merge($data, $this->args);
                }
            }
        }
    }
    
    protected function checkHmacAuth()
    {
        if (array_key_exists('Authorization', $this->requestHeaders) && $this->requestHeaders['Authorization']) {
            $authData = explode(':', $this->requestHeaders['Authorization']);
            $requestPublicId = substr($authData[0], strrpos($authData[0], ' ') + 1);
            
            list($publicId, $secret) = explode(':', $this->getExtConf('auth'));
            if ($requestPublicId == $publicId) {
                $hmacSign = new HmacSign($publicId, $secret);
                $hmacSign
                    ->setVerb($_SERVER['REQUEST_METHOD'])
                    ->setPath($_SERVER['REQUEST_URI'])
                    ->setTime(strtotime($this->requestHeaders['Date']))
                    ->setContentType($this->requestHeaders['Content-Type'])
                    ->setContentLength($this->requestHeaders['Content-Length'])
                    ->setContentMd5($this->requestHeaders['Content-MD5']);
    
                if ($hmacSign->checkAuth($authData[1])) {
                    return true;
                } else{
                    $this->render(array(
                        'errors' => array('Authentication request failed for publicId ' . $requestPublicId),
                    ), 401);
                }
            } else {
                $this->render(array(
                    'errors' => array('publicId ' . $requestPublicId . ' unknown'),
                ), 401);
            }
        } else {
            $this->render(array(
                'errors' => array('Authentification missing'),
            ), 401);
        }
    }

    /**
     * @return string
     */
    protected function getEnv()
    {
        return ($this->getExtConf('devMode') || getenv('TYPO3_CONTEXT_SYSTEM') == 'Development') ? 'dev' : 'prod';
    }

    /**
     * @param array $vars
     */
    protected function render($vars, $status = 200)
    {
        header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json');
        http_response_code($status);
        echo json_encode($vars);
        exit;
    }
}