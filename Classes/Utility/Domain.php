<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaT3um\Utility;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\ArrayUtility;

/**
 * @package     imia_t3um
 * @subpackage  Utility
 * @author      David Frerich <d.frerich@imia.de>
 */
class Domain
{
	static public function check()
	{
		$extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['imia_t3um']);
		$enviroment = ($extConf['devMode'] || GeneralUtility::getApplicationContext()->isDevelopment()) ? 'dev' : 'prod';
		$domain = $_SERVER['HTTP_HOST'];

		if ($domain) {
			list($publicId, $secret) = explode(':', $extConf['auth']);
			$instanceData = array(
				$publicId,
				PATH_site,
                gethostbyname(gethostname()),
				$domain,
				$enviroment,
			);

			$instanceDataAlt = $instanceData;
			$instanceDataAlt[4] = $enviroment == 'dev' ? 'prod' : 'dev';

            $instanceKey = sha1(implode(':', $instanceData));
			$instanceKeyAlt = sha1(implode(':', $instanceDataAlt));

			if ($extConf['instances']) {
				$instances = explode(',', $extConf['instances']);
                foreach ($instances as $key => $identifier) {
                    if (strlen($identifier) !== 40) {
                        unset($instances[$key]);
                    }
                }
			} else {
				$instances = array();
			}

			if (in_array($instanceKeyAlt, $instances)) {
				foreach ($instances as $key => $val) {
					if ($instanceKeyAlt == $val) {
						unset($instances[$key]);
					}
				}
			}

			if (!in_array($instanceKey, $instances)) {
				$httpClient = GeneralUtility::makeInstance('IMIA\ImiaT3um\Utility\Api\HttpClient',
					$extConf['serviceURL'], (int)$publicId, $secret);

				$domainInfo = $httpClient->addDomain($domain, $enviroment, PATH_site, gethostbyname(gethostname()));
				if ($domainInfo['success']) {
					$instances[] = $instanceKey;

					$objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
					$configurationUtility = $objectManager->get('TYPO3\CMS\Extensionmanager\Utility\ConfigurationUtility');
					$newConfiguration = $configurationUtility->getCurrentConfiguration('imia_t3um');

					ArrayUtility::mergeRecursiveWithOverrule($newConfiguration, array(
						'instances' => array('value' => implode(',', $instances)),
					));

					if (GeneralUtility::getApplicationContext()->isDevelopment()) {
						$newConfiguration['devMode']['value'] = '0';

						$extConfT3UM = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['imia_t3um']);
						$extConfT3UM['devMode'] = '0';
						$GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['imia_t3um'] = serialize($extConfT3UM);
					}

					ob_start();
					$configurationUtility->writeConfiguration(
						$configurationUtility->convertValuedToNestedConfiguration($newConfiguration),
						'imia_t3um'
					);
					ob_end_clean();
				}
			}
		}
	}
}