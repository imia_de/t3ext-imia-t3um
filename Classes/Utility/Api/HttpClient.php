<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaT3um\Utility\Api;

class HttpClient
{
    /**
     * @var string
     */
    protected $baseUrl;

    /**
     * @var HmacSign
     */
    protected $hmacSign;

    /**
     * @var array
     */
    protected $publicUserIds = array();

    /**
     * @var boolean
     */
    protected $debug = false;

    /**
     * @param string $baseUrl
     * @param string $publicId
     * @param string $secret
     */
    public function __construct($baseUrl, $publicId, $secret)
    {
        $this->hmacSign = new HmacSign($publicId, $secret);
        $this->baseUrl = $baseUrl;
    }

    /**
     * @return array
     */
    public function getInfo()
    {
        return $this->query();
    }

    /**
     * @param string $domain
     * @param string $enviroment
     * @param string $path
     * @param string $ip
     * @return array
     */
    public function addDomain($domain, $enviroment, $path, $ip)
    {
        return $this->query('domain', 'GET', array(
            'domain'        => $domain,
            'enviroment'    => $enviroment,
            'path'          => $path,
            'ip'            => $ip,
        ));
    }

    /**
     * @param string $domain
     * @param string $enviroment
     * @param string $path
     * @param string $ip
     * @return array
     */
    public function updateDomain($domain, $enviroment, $path, $ip)
    {
        return $this->query('domain', 'GET', array(
            'domain'        => $domain,
            'enviroment'    => $enviroment,
            'path'          => $path,
            'ip'            => $ip,
        ));
    }

    /**
     * @param array $instance
     * @return array
     */
    public function getExtensions($instance)
    {
        $result = $this->query($instance['slug'] . '/extensions');
        if (is_array($result) && array_key_exists('extensions', $result)) {
            $extensions = $result['extensions'];
        } else {
            $extensions = array();
        }

        return $extensions;
    }

    /**
     * @param array $extensionKeys
     * @return array
     */
    public function getTERExtensions($extensionKeys)
    {
        $result = $this->query('extensions', 'GET',
            array('keys' => $extensionKeys
        ));

        if (is_array($result) && array_key_exists('extensions', $result)) {
            $extensions = $result['extensions'];
        } else {
            $extensions = array();
        }

        return $extensions;
    }

    /**
     * @param string $publicId
     * @param string $secret
     * @return HmacSign
     */
    public function getHmacSignUtility($publicId = null, $secret = null)
    {
        return new HmacSign($publicId, $secret);
    }

    /**
     * @param string $action
     * @param string $method
     * @param array $data
     * @param mixed $result
     * @param mixed $info
     * @return array
     */
    public function query($action = '', $method = 'GET', $data = array(), &$result = null, &$info = null)
    {
        $action = 'api/hmac' . ($action ? '/' . $action : '');

        $time = time();
        $body = '';
        if (count($data) > 0) {
            if ($method == 'GET' && strpos($action, '?') === false) {
                $action .= '?' . http_build_query($data);
            } else {
                $body = json_encode($data);
            }
        }

        $hmac = $this->getHmacSign($action, $time, $body, $method);
        if (strlen($body) > 0) {
            $header = array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($body),
                'Content-MD5: ' . base64_encode(md5($body)),
                'Date: ' . gmdate('D, d M Y H:i:s \G\M\T', $time),
                'Authorization: ' . $hmac
            );
        } else {
            $header = array(
                'Content-Type: application/x-www-form-urlencoded',
                'Date: ' . gmdate('D, d M Y H:i:s \G\M\T', $time),
                'Authorization: ' . $hmac
            );
        }

        $ch = \curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->getUrl($action));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $body);

        switch ($method) {
            case 'POST':
                curl_setopt($ch, CURLOPT_POST, true);
                break;
            case 'GET':
                curl_setopt($ch, CURLOPT_POST, false);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
                break;
            case 'PUT':
                curl_setopt($ch, CURLOPT_POST, false);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
                break;
            case 'DELETE':
                curl_setopt($ch, CURLOPT_POST, false);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
                break;
        }

        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $result = curl_exec($ch);
        $info = curl_getinfo($ch);

        if ($this->debug) {
            echo '<pre>';
            print_r($result);
            echo '</pre>';
        }

        $response = json_decode($result, true);
        if (!$result){
            $curlError = curl_errno($ch);

            if ($curlError) {
                if (!is_array($response)) {
                    $response = array();
                }
                if (!array_key_exists('errors', $response)) {
                    $response['errors'] = array();
                }
                $response['errors'][] = curl_error($ch);
            }
        }

        switch ((int)$info['http_code']) {
            case 0:
                if (!is_array($response)) {
                    $response = array();
                }
                if (!array_key_exists('errors', $response)) {
                    $response['errors'] = array('Service not available');
                }
                break;
            case 404:
                if (!is_array($response)) {
                    $response = array();
                }
                if (!array_key_exists('errors', $response)) {
                    $response['errors'] = array();
                }

                $response['errors'][] = '404 - Not found';
                break;
            case 204:
                if (!is_array($response)) {
                    $response = array();
                }
                if (!array_key_exists('errors', $response)) {
                    $response['errors'] = array();
                }

                $response['errors'][] = '204 - No Content';
                break;
        }

        if (!$response) {
            $response = true;
        }

        curl_close($ch);

        return $response;
    }

    /**
     * @param string $action
     * @return string
     */
    protected function getUrl($action = '')
    {
        return $this->baseUrl . ($this->debug ? '/sf2_dev.php' : '') . ($action ? '/' . $action : '');
    }

    /**
     * @param string $path
     * @param integer $time
     * @param string $body
     * @param string $method
     * @param string $contentType
     * @return string
     */
    protected function getHmacSign($action, $time, $body, $method = 'POST', $contentType = 'application/x-www-form-urlencoded')
    {
        $hmac = $this->hmacSign
            ->setVerb($method)
            ->setPath('/' . $action)
            ->setTime($time)
            ->setContentType($contentType)
            ->setBody($body)
            ->getAuthHeader();

        $this->hmacSign->reset();

        return $hmac;
    }
}