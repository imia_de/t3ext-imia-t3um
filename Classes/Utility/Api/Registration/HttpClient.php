<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2015 IMIA net based solutions (info@imia.de)
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

namespace IMIA\ImiaT3um\Utility\Api\Registration;

use IMIA\ImiaT3um\Exception\Exception;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_t3um
 * @subpackage  Controller
 * @author      David Frerich <d.frerich@imia.de>
 */
class HttpClient
{
    /**
     * @var string
     */
    protected $baseUrl;

    /**
     * @var string
     */
    protected $user;

    /**
     * @var string
     */
    protected $password;

    /**
     * @var string
     */
    protected $sessionKey;

    /**
     * @var array
     */
    protected $header = array();

    /**
     * @var string
     */
    protected $cookieJar;

    /**
     * @var mixed
     */
    protected $login = false;

    /**
     * @param string $baseUrl
     * @param string $user
     * @param string $password
     */
    public function __construct($baseUrl, $user, $password)
    {
        $this->baseUrl = $baseUrl;
        $this->user = $user;
        $this->password = $password;
        $this->cookieJar = tempnam(PATH_site . 'typo3temp','cookie');
    }

    /**
     * @param string $sitename
     * @param string $encryptionKey
     * @return array
     * @throws Exception
     */
    public function register($sitename, $encryptionKey)
    {
        $return = $this->post('api/register', array(
            'name'          => $sitename,
            'encryptionKey' => $encryptionKey,
        ));
        
        return $return['response'];
    }

    /**
     * @param string $url
     * @return array
     * @throws Exception
     */
    public function get($url)
    {
        return $this->post($url);
    }

    /**
     * @param string $url
     * @param array $postFields
     * @return array
     * @throws Exception
     */
    public function post($url, $postFields = null)
    {
        if (!$this->login) {
            $error = $this->login();
            if ($error) {
                throw new Exception($error);
            }
        }

        $result = $this->request($url, $postFields);
        if ($result['response'] && $result['info']['http_code'] == 200) {
            return $result;
        } else {
            $error = $this->login();
            if ($error) {
                throw new Exception($error);
            }

            $result = $this->request($url, $postFields);
            if ($result['response'] && $result['info']['http_code'] == 200) {
                return $result;
            } else {
                $exception = new Exception();
                throw $exception->setObject($result);
            }
        }
    }

    /**
     * @return mixed
     */
    protected function login()
    {
        $error = 'Problem beim Aufbau der Verbindung';
        if ($this->user && $this->password) {
            $result = $this->request('login.json');

            if ($result['response'] && is_array($result['response']) 
                && $result['response']['csrf_token'] && $result['response']['url']) {

                $result = $this->request(preg_replace('/^\/(sf2_dev\.php\/)?/ism', '', $result['response']['url']), array(
                    '_csrf_token'   => $result['response']['csrf_token'],
                    '_username'     => $this->user,
                    '_password'     => $this->password,
                    '_remember_me'  => 'on',
                ));

                if ($result['response'] && is_array($result['response'])) {
                    if ($result['response']['success'] && $result['response']['user']) {
                        $error = false;
                        $this->login = time();
                    } elseif ($result['response']['error']) {
                        $error = $result['response']['error'];
                    }
                }
            }
        }

        return $error;
    }

    /**
     * @param string $url
     * @param array $postFields
     * @param array $cookies
     * @param array $headers
     * @return array
     */
    protected function request($url, $postFields = null) 
    {
        $this->header = array();

        $ch = \curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL             => $this->baseUrl . '/' . $url,
            CURLOPT_HEADER          => false,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_FOLLOWLOCATION  => true,
            CURLOPT_SSL_VERIFYHOST  => false,
            CURLOPT_SSL_VERIFYPEER  => false,
            CURLOPT_HEADERFUNCTION  => array($this, 'readHeader'),
        ));

        curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookieJar);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookieJar);

        if ($postFields) {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
        }
        
        $response = curl_exec($ch);
        $info = curl_getinfo($ch);

        if ($response && $this->header['Content-Type'] == 'application/json') {
            $jsonResponse = json_decode($response, true);
            
            if (is_array($jsonResponse)) {
                $response = $jsonResponse;
                
                if ($response['error']) {
                    if (is_string($response['error'])) {
                        throw new Exception($response['error']);
                    } else {
                        $exception = new Exception();
                        throw $exception->setObject($response['error']);
                    }
                }
            }
        }

        curl_close($ch);

        return array(
            'response'  => $response,
            'header'    => $this->header,
            'info'      => $info
        );
    }

    /**
     * @param object $ch
     * @param string $headerLine
     * @return integer
     */
    public function readHeader($ch, $headerLine)
    {
        if (trim($headerLine)) {
            $headerInfo = explode(':', $headerLine, 2);
            
            if (count($headerInfo) == 2) {
                $this->header[$headerInfo[0]] = trim($headerInfo[1]);
            } else {
                $this->header[] = trim($headerInfo[0]);
            }
        }

        return strlen($headerLine);
    }
}