<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaT3um\Utility\Api;

/**
 * @package     imia_t3um
 * @subpackage  Controller
 * @author      David Frerich <d.frerich@imia.de>
 */
class HmacSign
{
    const AUTH_NAME = "T3UMEXT";
    const ALGORITHM = "sha256";

    /**
     * @var string
     */
    protected $publicId;

    /**
     * @var string
     */
    protected $secret;

    /**
     * @var string
     */
    protected $path = '';

    /**
     * @var string
     */
    protected $verb = '';

    /**
     * @var integer
     */
    protected $time = 0;

    /**
     * @var integer
     */
    protected $contentLength = 0;

    /**
     * @var string
     */
    protected $contentType = '';

    /**
     * @var string
     */
    protected $contentMD5 = '';

    /**
     * @param string $publicId
     * @param string $secret
     */
    public function __construct($publicId, $secret)
    {
        $this->publicId = $publicId;
        $this->secret = $secret;
    }

    /**
     * @return string
     */
    public function getAuthHeader()
    {
        $sign = $this->calculateHMAC($this->getHeaderData());
        $authHeader = self::AUTH_NAME . ' ' . $this->publicId . ':' . $sign;

        return $authHeader;
    }

    public function reset()
    {
        $this->path = '';
        $this->verb = '';
        $this->time = 0;
        $this->contentLength = 0;
        $this->contentType = '';
        $this->contentMD5 = '';
    }

    /**
     * @param string $sign
     * @return bool
     */
    public function checkAuth($sign)
    {
        $success = false;
        if ($this->calculateHMAC($this->getHeaderData()) === $sign) {
            $success = true;
        }

        return $success;
    }

    /**
     * @param string $secret
     * @return $this
     */
    public function setSecret($secret)
    {
        $this->secret = $secret;

        return $this;
    }

    /**
     * @param string $publicId
     * @return $this
     */
    public function setPublicId($publicId)
    {
        $this->publicId = $publicId;

        return $this;
    }

    /**
     * @param string $verb
     * @return $this
     */
    public function setVerb($verb)
    {
        $this->verb = $verb;

        return $this;
    }

    /**
     * @param string $path
     * @return $this
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * @param integer $time
     * @return $this
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * @param integer $contentLength
     * @return $this
     */
    public function setContentLength($contentLength)
    {
        $this->contentLength = $contentLength;

        return $this;
    }

    /**
     * @param string $contentMD5
     * @return $this
     */
    public function setContentMD5($contentMD5)
    {
        $this->contentMD5 = $contentMD5;

        return $this;
    }

    /**
     * @param string $contentType
     * @return $this
     */
    public function setContentType($contentType)
    {
        $this->contentType = $contentType;

        return $this;
    }

    /**
     * @param string $body
     */
    public function setBody($body)
    {
        $this
            ->setContentLength(strlen($body))
            ->setContentMD5($body ? base64_encode(md5($body)) : '')
        ;

        return $this;
    }

    /**
     * @param string $data
     * @return string
     */
    public function calculateHMAC($data)
    {
        return base64_encode(hash_hmac(self::ALGORITHM, $data, $this->secret, true));
    }

    /**
     * @return string
     */
    protected function getHeaderData()
    {
        $data = array(
            $this->contentType,
            $this->contentLength ?: '',
            $this->contentMD5 ?: '',
            gmdate('D, d M Y H:i:s \G\M\T', $this->time),
            $this->verb,
            $this->path,
        );

        return implode("\n", $data) . "\n";
    }
}