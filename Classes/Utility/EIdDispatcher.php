<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2015 IMIA net based solutions (info@imia.de)
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

namespace IMIA\ImiaT3um\Utility;

putenv('TYPO3_CONTEXT_SYSTEM=' . getenv('TYPO3_CONTEXT'));
putenv('TYPO3_CONTEXT=Production');

if (function_exists('apache_setenv')) {
    apache_setenv('TYPO3_CONTEXT_SYSTEM', getenv('TYPO3_CONTEXT_SYSTEM'));
    apache_setenv('TYPO3_CONTEXT', 'Production');
}

$dispatcher = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
    'IMIA\ImiaT3um\Utility\Dispatcher');

$dispatcher
    ->init()
    ->setVendorName('IMIA')
    ->setExtensionName('ImiaT3um')
    ->setPluginName('api')
    ->setControllerName('Api')
    ->setActionName($_REQUEST['action'] ?: 'info')
    ->dispatch();