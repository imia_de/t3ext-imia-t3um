<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaT3um\Utility;

use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @package     imia_t3um
 * @subpackage  Utility
 * @author      David Frerich <d.frerich@imia.de>
 */
class Dispatcher
{
	/**
	 * @var string
	 */
	protected $vendorName;

	/**
	 * @var string
	 */
	protected $extensionName;

	/**
	 * @var string
	 */
	protected $pluginName;

	/**
	 * @var string
	 */
	protected $controllerName;

	/**
	 * @var string
	 */
	protected $actionName;

	/**
	 * Called by eID
	 * Builds an extbase context and returns the response
	 */
	public function dispatch()
	{
		$configuration = array(
			'vendorName'    => $this->vendorName,
			'extensionName' => $this->extensionName,
			'pluginName'    => $this->pluginName,
			'switchableControllerActions'   => array(
				$this->controllerName   => array($this->actionName),
			)
		);

		$bootstrap = GeneralUtility::makeInstance(
			'TYPO3\CMS\Extbase\Core\Bootstrap');

		echo $bootstrap->run('', $configuration);
	}

	/**
	 * @param integer $pageUid
	 * @return \IMIA\ImiaBaseExt\Utility\AjaxDispatcher
	 */
	public function init($pageUid = 0)
	{
		$GLOBALS['TSFE'] = GeneralUtility::makeInstance(
			'TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController', $GLOBALS['TYPO3_CONF_VARS'], $pageUid, 0, 1);
		$GLOBALS['TSFE']->sys_page = GeneralUtility::makeInstance(
			'TYPO3\CMS\Frontend\Page\PageRepository');

		$GLOBALS['TSFE']->initFEuser();
		$GLOBALS['TSFE']->id = $pageUid;
		$GLOBALS['TSFE']->determineId();
		$GLOBALS['TSFE']->getCompressedTCarray();
		$GLOBALS['TSFE']->initTemplate();
		$GLOBALS['TSFE']->getConfigArray();
		$GLOBALS['TSFE']->includeTCA();

		return $this;
	}

	/**
	 * @param string $vendorName
	 * @return \IMIA\ImiaBaseExt\Utility\AjaxDispatcher
	 */
	public function setVendorName($vendorName)
	{
		$this->vendorName = $vendorName;

		return $this;
	}

	/**
	 * @param string $extensionName
	 * @return \IMIA\ImiaBaseExt\Utility\AjaxDispatcher
	 */
	public function setExtensionName($extensionName)
	{
		$this->extensionName = $extensionName;

		return $this;
	}

	/**
	 * @param string $pluginName
	 * @return \IMIA\ImiaBaseExt\Utility\AjaxDispatcher
	 */
	public function setPluginName($pluginName)
	{
		$this->pluginName = $pluginName;

		return $this;
	}



	/**
	 * @param string $controllerName
	 * @return \IMIA\ImiaBaseExt\Utility\AjaxDispatcher
	 */
	public function setControllerName($controllerName)
	{
		$this->controllerName = $controllerName;

		return $this;
	}



	/**
	 * @param string $actionName
	 * @return \IMIA\ImiaBaseExt\Utility\AjaxDispatcher
	 */
	public function setActionName($actionName)
	{
		$this->actionName = $actionName;

		return $this;
	}

}