<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2015 IMIA net based solutions (info@imia.de)
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

namespace IMIA\ImiaT3um\ViewHelpers\Version;

/**
 * @package     imia_t3um
 * @subpackage  ViewHelpers
 * @author      David Frerich <d.frerich@imia.de>
 */
class CompareViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper
{
    /**
     * @param string $version
     * @param string $compare
     * @return string
     */
    public function render($version, $compare = null)
    {
        if (!$compare) {
            $compare = $this->renderChildren();
        }

        $version = $this->versionToFloat($version);
        $compare = $this->versionToFloat($compare);

        if ($version == $compare) {
            return '=';
        } elseif ($version > $compare) {
            return '<';
        } else {
            return '>';
        }
    }

    /**
     * @param string $version
     * @return float
     */
    protected function versionToFloat($version)
    {
        $float = 0;
        $multi = 1000000;
        $div = 1000;
        foreach (explode('.', $version) as $part) {
            $float += (int)$part * $multi;
            $multi = $multi / $div;
        }

        return (float)$float;
    }
}