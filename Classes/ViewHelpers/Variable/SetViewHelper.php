<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaT3um\ViewHelpers\Variable;

use TYPO3\CMS\Extbase\Reflection\ObjectAccess;

/**
 * @package     imia_t3um
 * @subpackage  ViewHelpers
 * @author      David Frerich <d.frerich@imia.de>
 */
class SetViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper
{
	/**
	 * @param string $name
	 * @param mixed $value
	 * @return void
	 */
	public function render($name, $value = null) 
    {
		if (!$value) {
			$value = $this->renderChildren();
		}

		if (!strpos($name, '.')) {
			if ($this->templateVariableContainer->exists($name)) {
				$this->templateVariableContainer->remove($name);
			}
			$this->templateVariableContainer->add($name, $value);
		} elseif (substr_count($name, '.') === 1) {
			$parts = explode('.', $name);
			$objectName = array_shift($parts);
			$path = implode('.', $parts);

			if (!$this->templateVariableContainer->exists($objectName)) {
				return null;
			}
			$object = $this->templateVariableContainer->get($objectName);

			try {
				ObjectAccess::setProperty($object, $path, $value);

				$this->templateVariableContainer->remove($objectName);
				$this->templateVariableContainer->add($objectName, $object);
			} catch (\Exception $error) {
				return null;
			}
		}

		return null;
	}

}
