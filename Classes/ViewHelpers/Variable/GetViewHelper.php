<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 IMIA net based solutions (info@imia.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace IMIA\ImiaT3um\ViewHelpers\Variable;

use TYPO3\CMS\Extbase\Reflection\ObjectAccess;

/**
 * @package     imia_t3um
 * @subpackage  ViewHelpers
 * @author      David Frerich <d.frerich@imia.de>
 */
class GetViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper
{
	/**
	 * @param string $name
	 * @param boolean $useRawKeys
	 * @return mixed
	 */
	public function render($name, $useRawKeys = FALSE) 
    {
		if (!strpos($name, '.')) {
			if ($this->templateVariableContainer->exists($name)) {
				return $this->templateVariableContainer->get($name);
			}
		} else {
			$segments = explode('.', $name);
			$templateVariableRootName = $lastSegment = array_shift($segments);
			if ($this->templateVariableContainer->exists($templateVariableRootName)) {
				$templateVariableRoot = $this->templateVariableContainer->get($templateVariableRootName);
				if ($useRawKeys) {
					return ObjectAccess::getPropertyPath($templateVariableRoot, implode('.', $segments));
				}

				try {
					$value = $templateVariableRoot;
					foreach ($segments as $segment) {
						if (ctype_digit($segment)) {
							$segment = intval($segment);
							$index = 0;

							foreach ($value as $possibleValue) {
								if ($index === $segment) {
									$value = $possibleValue;
									break;
								}
								$index++;
							}
						} else {
						    $value = ObjectAccess::getProperty($value, $segment);
                        }
					}
					return $value;
				} catch (\Exception $e) {
					return null;
				}
			}
		}
        
		return null;
	}
}
